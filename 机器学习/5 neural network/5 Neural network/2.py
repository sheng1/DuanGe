"""
神经网络训练模型
结构：输入层->隐藏层->输出层(最基本简单的结构，非卷积)
注意：
1. 该程序神经网络的节点参数为w_ih和w_ho，不会保存。
2. 该程序仅运行作为结果查看模型的好坏，不会保存任何节点数据。
每次重新运行的都是初始数据。
3. 该程序最初用于训练手写数字，训练的文件说明：
    //1//mnist_train.csv: 训练全部数据集，包含60000张手写数字图片(大概运行5分钟)
    //2//mnist_train_100.csv: 训练数据集，为其中的100张图片(运行瞬间完成)
    //3//mnist_test_10.csv: 测试数据集，包含10章图片(运行瞬间完成)
4. 在训练垃圾识别时，输入图片的要求：
    //1//每张图片的像素点必须相同
    //2//必须的灰度图片
5. 一点建议：
    //1//暂时没有。。。到时候讨论吧

"""
import numpy
import scipy.special
import tqdm

# 定义神经网络
class NeuralNetwork:
    # 初始化函数
    def __init__(self, inputNodes, hiddenNodes, outputNodes, learningRate):
        # 设置输入层、隐藏层、输出层
        self.inputNodes = inputNodes
        self.hiddenNodes = hiddenNodes
        self.outputNodes = outputNodes
        # 设置学习率
        self.learningRate = learningRate
        # 初始化权重矩阵(方差为0的随机数)
        self.w_ih = numpy.random.normal(0.0, pow(self.hiddenNodes,-0.5),
                                        (self.hiddenNodes, self.inputNodes))
        self.w_ho = numpy.random.normal(0.0, pow(self.outputNodes,-0.5),
                                        (self.outputNodes, self.hiddenNodes))
        # 初始化激活函数(s函数)
        self.activate = lambda x: scipy.special.expit(x)
        pass

    # 训练网络
    def train(self, inputList, targetList):
        '''
        训练第一步：针对训练样本给出输出
        '''
        # 输入数据矩阵、目标结果矩阵
        inputs = numpy.array(inputList, ndmin=2).T
        targets = numpy.array(targetList, ndmin=2).T
        # 隐藏层输入
        hiddenInput = numpy.dot(self.w_ih, inputs)
        # 隐藏层输出
        hiddenOutput = self.activate(hiddenInput)
        # 最终输入
        finalInput = numpy.dot(self.w_ho, hiddenOutput)
        # 最终输出
        finalOutput = self.activate(finalInput)
        '''
        训练第二步：针对输出，计算误差，根据误差反向更新隐藏层神经的误差，进而 权重。
        '''
        # 计算输出误差
        outputError = targets - finalOutput
        print(sum(outputError))
        # 计算反向隐藏层误差
        hiddenError = numpy.dot(self.w_ho.T, outputError)
        # 更新隐藏层输出的权重
        self.w_ho += self.learningRate * numpy.dot(
            outputError * finalOutput * (1.0 - finalOutput), hiddenOutput.T)
        # 更新输入隐藏层的权重
        self.w_ih += self.learningRate * numpy.dot(
            hiddenError * hiddenOutput * (1.0 - hiddenOutput), inputs.T)
        pass

    # 查询网络
    def query(self, inputList):
        # 输入数据矩阵
        inputs = numpy.array(inputList, ndmin=2).T
        # 隐藏层输入
        hiddenInput = numpy.dot(self.w_ih, inputs)
        # 隐藏层输出
        hiddenOutput = self.activate(hiddenInput)
        # 最终输入
        finalInput = numpy.dot(self.w_ho, hiddenOutput)
        # 最终输出
        finalOutput = self.activate(finalInput)
        return finalOutput

def main():
    # 初始化设置
    """
    inputNodes: 输入层的数量，取决于输入图片(训练和识别)的像素，像素点的数量等于输入层的数量
    hiddenNodes: 隐藏层的数量，一般比输入层少，但具体不确定，可根据准确率进行调整
    outputNodes: 输出层的数量，等于需要分类的数量。
    """
    inputNodes = 307200
    hiddenNodes =250
    outputNodes = 10
    # 学习率的设置
    """
    学习率越大，每次学习变化越大，改变的越多，但接近准确点时无法准确到达
    学习率越小，每次学习更改的参数值就越小，只有大量的数据才能习得准确值
    """
    learningRate =0.001
    # 生成神经网络
    neuralTest = NeuralNetwork(inputNodes, hiddenNodes, outputNodes, learningRate)


    #================================训练网络===============================

    # 读取图像CSV文件
    """
    输入文件为CSV文件，是一个存放图片数据集的文件。
    文件要求：
    1. 文件每一行为一张图片的数据。
    2. 每一行图片的开头为图片的识别答案(可以设置不同数字代表不同的垃圾)
    3. 每个数字用逗号隔开。
    ******4. 每张图片的像素点必须相同******
    """
    file = open('rubbish_train.csv', 'r') # 这个是训练手写数字的文件
    fileData = file.readlines()
    file.close()
    # 训练数据集用于训练的次数
    trainTimes = 6
    # 多次的利用相同数据训练
    qbar = tqdm.tqdm(total=5)
    for e in range(trainTimes):
        qbar.update(1)
        # 遍历循环每个图片数据矩阵
        tqbar = tqdm.tqdm(total=100)
        for record in fileData:
            tqbar.update(10)
            # 将图片用逗号分隔的数据转换为序列(矩阵)
            imageValue = record.split(',')
            # 将图片转换为double形式并视为输入值,asfarray移除符号作用
            inputs = numpy.asfarray(imageValue[1:]) / 255.0 * 0.99 + 0.01
            # 初始化目标值
            targets = numpy.zeros(outputNodes) + 0.01
            # 初始化最正确的目标节点
            targets[int(imageValue[0])] = 0.99
            # 训练网络
            neuralTest.train(inputs, targets)
            pass
        pass


    #================================测试网络===============================

    # 读取图像CSV文件
    testFile = open('rubbish_test.csv', 'r')   # 这个是训练手写数字的文件
    testData = testFile.readlines()

    file.close()
    # 初始化查询情况矩阵，识别正确填入1，否则填入0
    neuralScore = []

    # 开始遍历所有图片数据测试
    for record in testData:
        # 将图片用逗号分隔的数据转换为序列(矩阵)
        testImageData = record.split(',')
        # 根据数据集采集的正确答案
        answer = int(testImageData[0])

        print('正确的识别答案是：', answer)
        # 将图片变为double形式
        inputs = numpy.asfarray(testImageData[1:]) / 255.0 * 0.99 + 0.01
        # 查询网络识别结果：
        replyOutput = neuralTest.query(inputs)
        neuralReply = numpy.argmax(replyOutput)
        print('网络查询的结果：', neuralReply)
        # 结果填入查询情况矩阵
        if answer==neuralReply :
            neuralScore.append(1)
        else:
            neuralScore.append(0)
        pass

    # 计算查询结果的正确率
    rightMatrix = numpy.asarray(neuralScore)
    rightRate = rightMatrix.sum() / rightMatrix.size
    print('识别准确率：', rightRate)


if __name__ == '__main__':
    main()
