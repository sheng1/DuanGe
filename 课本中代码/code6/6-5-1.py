class num(object):
    def __init__(self, value):
        self.value = value
    def get_neg(self):
        return -self.value
    def set_neg(self, value):
        self.value = -value
    def del_neg(self):
        print("value also deleted")
        del self.value
    neg = property(get_neg, set_neg, del_neg, "I'm negative")
x = num(1.1)
print(x.neg)              # 打印-1.1
x.neg = -22
print(x.value)            # 打印22
print(num.neg.__doc__)    # 打印"I'm negative"
del x.neg                 # 打印"value also deleted"
