a=[1,2,3,4,5,6,3,4,6]
print(a.count(3))  #a.count(3)就是计数a列表（list）里面有多少个元素3
print(a.index(3))  #a.index(3)就是查看元素3第一次出现时的下标，其中从0开始，这里的2表示3在第三个元素
print(a.append(6)) #a.append(6)表示在列表的最后添加一个新元素6
print(a)   #通过打印我们可以看出列表a添加了一个新元素6
print(a.sort()) #排序
print(a)
print(a.reverse()) #颠倒次序
print(a)
print(a.pop()) #除去最后一个元素，并将该元素返回
print(a)
print(a.remove(2)) #除去第一次出现的元素2
print(a)
print(a.insert(0,9))  #在0的位置插入元素9
print(a)
print(a.clear())
print(a)