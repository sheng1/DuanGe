class bird(object):
    def chirp(self,sound):       #我们这里给这个鸟类定义了一个公共属性，就是所有的鸟类都会叫。
        print(sound)             #这里说明了只要是属于鸟类的，都打印出叫声。
    def set_color(self,color):   #我们这里给不同的鸟设立一个属于它们的个性颜色
            self.color=color

summer=bird()         #summer属于鸟类
summer.set_color("yellow")  #summer的颜色是黄色
print(summer.color)    #打印颜色
summer.chirp("jijiji") #鸟的叫声是jijiji
