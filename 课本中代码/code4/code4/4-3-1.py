
#源代码
class Bird(object):
    def __init__(self,sound):  #类的 __init__()方法，Python会在每次创建对象时自动调用(特别注意这里是两个下划线）。
        self.sound=sound       #我们在创建对象的时候，__init__()方法将会被调用。
        print("my sound is",sound)
    def chirp(self):
        print(self.sound)


summer = Bird("ji") #创建对象，并初始化叫声
summer.chirp()  #summer属于鸟，打印出鸟的叫声。