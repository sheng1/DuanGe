#example_tuple=(2,1.3,"love",5.6,9,12,false)
example_list=[5,2]
print(example_list[1])

tuple[:5]  #从下标0到下标4，不包含下标5

tuple[2:]  #从下标2到最后一个元素

tuple[0:5:2]   #下标为0，2，4的元素

tuple[-1]   #序列最后一个元素

tuple[-3]  #序列倒数第三个元素

tuple[1:-1]   #序列的第二个元素到倒数第二个元素