
'''v="vivian"
print(v)

total=86000
requirement=total*(0.15+0.2)
print(requirement)

a=100  #整型
a=100.0 #浮点型
a='abc' #字符串。也可以使用双引号"abc"标记字符串。
a=true #布尔值

example_tuple=(2,1.3,"love",5.6,9,12,false) #元组
example_list=[true,5,"smile"] #列表
type(example_tuple) #结果为'tuple'
type(example_list) #结果为'list'

example_tuple[0] #结果为2
'''






#rate={"premium":0.2,"tax":0.15}  #可以用字典储存松散的数据

#如果产品售价超过50，交易费率为0.1，否则为0.2。下面写一段程序
'''total=98
if total > 50:
    rate = 0.01
else:
    rate = 0.02
print(rate)'''  #结果为0.01

'''total=98
if total > 50:
    print("总价超过50")
    rate = 0.01
else:
    print("总价不超过50")
    rate = 0.02
print(rate)  '''



#if的嵌套与elif
'''i=2
if i >0:     #条件1。如果条件成立则执行这部分  （条件1执行了，以下条件均不执行）
    print("positive i")
    i=i+1
elif i >1:   #条件2。不执行
    print("i is 0")
    i=i*10
else:        #条件3。不执行
    print("negetive i")
    i=i+1'''



'''i=2
if i >0:     #条件1。如果条件成立则执行这部分  （条件1执行了，以下符合条件均执行）
    print("positive i")
    i=i+1
    if i >1:   #条件2。执行
     print("i is 0")
     i=i*10
else:        #条件3。不执行
    print("negetive i")
    i=i+1'''

#if包含是同时打印出符合条件的结果
'''i=5
if i>3:
    print("good")
    if i>4:
        print("batter")'''




#example_tuple=(2,1.3,"love",5.6,9,12,false)
'''example_list=[5,2]
print(example_list[1])

tuple[:5]  #从下标0到下标4，不包含下标5

tuple[2:]  #从下标2到最后一个元素

tuple[0:5:2]   #下标为0，2，4的元素

tuple[-1]   #序列最后一个元素

tuple[-3]  #序列倒数第三个元素

tuple[1:-1]   #序列的第二个元素到倒数第二个元素'''

'''dict={"tom":11,"sam":57,"lily":100}
type(dict)   #结果为'dict'
print(dict["tom"])
dict["tom"]=29
print(dict)  '''  #如果代码过于长，可以不用返回原来字典的代码行进行修改



'''from random import choice   #random函数的调用
i=("1","2","3")
print(choice(i))'''


'''from random import shuffle
i=[1,2,3,4]
shuffle(i)
print(i)'''



'''i=0  #年份
x=500000
t=30000
tuple=(0.01,0.02,0.03,0.035)
r=0.05

while x>0:
    i=i+1
    print("第",i,"年要还是要还钱")
    if i<=4:
         for o in tuple:
          interest=o
    else:
         interest=r
    x=x*(1+interest)-t
print("一共需要",i,"年")




i=0
residual=500000
interest_tuple=(0.01,0.02,0.03,0.035)
repay=30000
while residual>0:
    i=i+1
    if i <=4:
        interest=interest_tuple[i-1]
    else:
        interest=0.05
    residual=residual*(interest+1)-repay
print(i+1)




i=0  #年份
residual=500000
interest_tuple=(0.01,0.02,0.03,0.035)
repay=30000
while residual>0:
    i=1
    if i <=4:
      interest=interest_tuple[0]
    else:
        interest=0.05
    residual=residual*(interest+1)-repay
print(i+1)'''

'''i=0  #年份
x=500000   #还款额
t=30000    #每次还款的金额
tuple=(0.01,0.02,0.03,0.035)  #元组  前四年的利率，每一年都不一样
r=0.05    #第五年开始就不变

while x>0:
    i=i+1
    print("第",i,"年要还是要还钱")
    if i<=4:
         for o in tuple:    #设一个变量o
          interest=o      #将利率进行赋值
    else:
         interest=r
    x=x*(1+interest)-t
print("一共需要",i+1,"年")  '''



'''def square_sum(a,b):
    a=a**2
    b=b**2
    c=a*b
    return c
x=square_sum(1,2)
print(x)    #结果为4

def square_sum(a,b):
    a=a+2
    b=b**2
    c=a+b
    return c
x=square_sum(1,2)
print(x)   #结果为7

def square_sum(a,b):
    a=a*2
    b=b**2
    c=a+b
    return c
x=square_sum(1,2)
print(x)   #结果为6

def square_sum(a,b):
    a=a**2
    b=b**2
    c=a+b
    return c
x=square_sum(1,2)
print(x)   #结果为5'''



'''x=max(1,2,3,4)
print(x)'''

#这是一串有灵魂的代码
'''a,b=eval(input("请输入a,b的值"))

def f(a,b):
    a=a*2
    b=b**2
    c=a+b
    return c   #主要是def和return函数的作用
x=f(a,b)
print(x)

#用f替换square_sum,效果一样，说明它们不是一个函数

a,b=eval(input("请输入a,b的值"))
def square_sum(a,b):
    a=a*2
    b=b**2
    c=a+b
    return c
x=square_sum(a,b)
print(x) '''

'''def print_arguments(a,b,c):
    print(a,b,c)
print_arguments(1,3,5)'''

'''def f (a,b,c=10):
    return a+b+c
print(f(3,2,1))   #把1赋值了给c.结果为6

def f (a,b,c=10):
    return a+b+c
print(f(3,2))  #结果为15'''

'''def package_position(*all_arguments):
    print(type(all_arguments))
    print(all_arguments)
package_position(1,4,6)
package_position(5,6,7,1,2,3)'''


'''sum=0
for i in range(1,100):
    sum=sum+i
print(sum)'''

'''def gaussian_sum(n):
    if n == 1:
        return 1
    else:
        return n + gaussian_sum(n-1)
print(gaussian_sum(100))'''


'''b=[3,5,6]
def change_list(b):
    b[0]=b[0]+1
    return b
print(change_list(b))
print(b)'''














