import http.client
conn=http.client.HTTPConnection("www.example.com")  #主机地址
conn.request("GET","/")   #请求方法和资源路径
response=conn.getresponse()
print(response.status,response.reason)#回复的状态码和状态描述
content=response.read()
print(content)