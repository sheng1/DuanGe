#常规文件操作   在5-3-1我们已经做了实验，现在我们在第6行后面将文件打开，重新写入
f = open("ss","a")
print(f.closed)  #检查文件是否打开
f.write("hello world\r\n")
f.close()   #关闭文件
print(f.closed) #打印True，此时文件已经关了
f=open("ss","a")
f.write("写入")    #程序成功