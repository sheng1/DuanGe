s="python"                             #将字符串python赋值于s
while s !="":                          #当s不等于空值的时候，无限循环。
   for i in s:                           #i遍历循环字符串s，即将“p,y,t,h,o,m"赋予i
     if i == "h":                            #当i等于字符串h时为条件1
      break                              #当符合条件1时，跳出当前内循环
   print( i, end="" )                #打印i，且打印结果不换行显示
   s=s[:-1]                          #当第一次循环时，打印出i=py,但此时s="python".  经过s=s[:-1]，根据切片原理-1="n",则读取到第二次s="pytho",如此类推
